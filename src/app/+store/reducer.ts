import {  ActionReducerMap, createReducer, on } from '@ngrx/store';
import {decrement, increment} from './actions';


export interface IAppState {
  global: IState;
}
export interface IState {
  counter: number;
}

const initialState: IState = {
  counter: 0,
};

export const reducer = createReducer<IState>(
  initialState,
  on(increment, (state, { amount }) => {
    return { ...state, counter: state.counter + amount };
  }),
  on(decrement, (state, { amount }) => {
    return { ...state, counter: state.counter - amount };
  }),
);


export const reducers: ActionReducerMap<IAppState> = { global: reducer };
