import { createSelector, createFeatureSelector } from '@ngrx/store';
import { IState } from './reducer';



export const getGlobal = createFeatureSelector<IState>('global');
export const getGlobalCounter = createSelector(getGlobal, g => g.counter);

