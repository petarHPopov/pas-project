import {Component, OnInit, OnDestroy} from '@angular/core';
import {AuthService} from '../core/services/auth.service';
import {Subscription} from 'rxjs';
import {PrimeNGConfig} from 'primeng/api';
import {Router} from '@angular/router';


@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit, OnDestroy {
  public visibleSidebar;
  public logged: boolean;
  public loggedSubscription: Subscription;



  constructor(
    private  authService: AuthService,
    private primengConfig: PrimeNGConfig,
    private  router: Router,

  ) {
  }

  ngOnInit(): void {
    this.primengConfig.ripple = true;
    this.loggedSubscription = this.authService.isLogged$.subscribe(state => this.logged = state);
  }

  ngOnDestroy(): void {
    this.loggedSubscription.unsubscribe();
  }

  toggleLogin(): void {
    if (this.logged) {
      this.authService.logout();
    } else {
      this.authService.login();
    }
  }

  loadTodos(): void {
    this.router.navigate(['todos/allTodos']);
  }

  display(): void {
    this.visibleSidebar = false;
  }

  callAll(): void {
    this.display();
    this.loadTodos();
  }

}

