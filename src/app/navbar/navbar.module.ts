import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NavbarComponent} from './navbar.component';
import {PrimeeNgModule} from '../prime-ng/prime-ng.module';



@NgModule({
  declarations: [NavbarComponent],
  imports: [
    CommonModule,
    PrimeeNgModule,

  ],
  exports: [NavbarComponent]
})
export class NavbarModule {
}
