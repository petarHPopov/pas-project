export class CreateTodo {
  name: string;
  due: string;
  isDone: boolean;
  id?: number;
}
