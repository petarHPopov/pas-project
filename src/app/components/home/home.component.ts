import {Component} from '@angular/core';
import {select, Store} from '@ngrx/store';
import {decrement, increment} from '../../+store/actions';
import {getGlobalCounter} from '../../+store/selectors';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {

  counter$ = this.store.pipe(select(getGlobalCounter));

  constructor(private store: Store) {
  }

  incrementCount(): void {
    this.store.dispatch(increment({amount: 10 } ));
  }

  decrementCount(): void{
    this.store.dispatch(decrement({amount: 10}));
  }
}
