import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SidebarModule} from 'primeng/sidebar';
import {MenuModule} from 'primeng/menu';
import {PanelModule} from 'primeng/panel';
import {AccordionModule} from 'primeng/accordion';
import {SplitButtonModule} from 'primeng/splitbutton';
import {ToolbarModule} from 'primeng/toolbar';
import {MenubarModule} from 'primeng/menubar';
import {InputTextModule} from 'primeng/inputtext';
import {CardModule} from 'primeng/card';
import {ButtonModule} from 'primeng/button';
import {MegaMenuModule} from 'primeng/megamenu';
import {CheckboxModule} from 'primeng/checkbox';
import {CalendarModule} from 'primeng/calendar';
import {FieldsetModule} from 'primeng/fieldset';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    SidebarModule,
    ButtonModule,
    MenuModule,
    PanelModule,
    AccordionModule,
    SplitButtonModule,
    ToolbarModule,
    MegaMenuModule,
    MenubarModule,
    InputTextModule,
    CardModule,
    CheckboxModule,
    CalendarModule,
    FieldsetModule,
  ],
  exports: [

    SidebarModule,
    ButtonModule,
    MenuModule,
    PanelModule,
    AccordionModule,
    SplitButtonModule,
    ToolbarModule,
    MegaMenuModule,
    MenubarModule,
    InputTextModule,
    CardModule,
    CheckboxModule,
    CalendarModule,
    FieldsetModule,
  ]
})
export class PrimeeNgModule {
}
