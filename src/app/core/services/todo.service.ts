import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Todo} from '../../types/todo.model';
import {Observable} from 'rxjs';

@Injectable()
export class TodoService {

  constructor(
    private readonly http: HttpClient) {
  }

  public getAllTodos(): Observable<Todo[]> {
    return this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos');

  }

}
