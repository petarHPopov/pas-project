import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Router} from '@angular/router';

@Injectable()
export class AuthService {

  private isLoggedSubject$ = new BehaviorSubject<boolean>(false);

  constructor(private readonly router: Router) {
  }

  // tslint:disable-next-line:typedef
  public get isLogged$() {
    return this.isLoggedSubject$.asObservable();
  }

  public login(): void {
    this.isLoggedSubject$.next(true);
  }

  public logout(): void {
    this.isLoggedSubject$.next(false);
    this.router.navigate(['/home']);
  }
}
