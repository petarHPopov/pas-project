import {Todo} from '../types/todo.model';

// @ts-ignore
export const todos: Todo[] = [

  {
    userId: 1,
    id: 0,
    title: 'CODIX Induction',
    completed: true
  },
];
