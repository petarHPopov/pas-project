import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListTodosComponent} from './list-todos/list-todos.component';
import {RouterModule} from '@angular/router';
import {SingleTodoComponent} from './single-todo/single-todo.component';
import {FormsModule} from '@angular/forms';
import {CreateTodoComponent} from './create-todo/create-todo.component';
import {PrimeeNgModule} from '../prime-ng/prime-ng.module';
import {TodoService} from '../core/services/todo.service';
import {HttpClientModule} from '@angular/common/http';
import {TodosFacade} from './facade/facade';
import {EffectsModule} from '@ngrx/effects';
import {TodoEffects} from './effects';
import {StoreModule} from '@ngrx/store';
import { todoReducers} from './reducer';


@NgModule({
  declarations: [ListTodosComponent, SingleTodoComponent, CreateTodoComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([
      {path: 'allTodos', component: ListTodosComponent},
      // { path: ':id/ditails',  component:  },
    ]),
    FormsModule,
    PrimeeNgModule,
    HttpClientModule,
    StoreModule.forFeature('todos', todoReducers),
    EffectsModule.forFeature([
      TodoEffects,
    ]),
  ],
  providers: [TodoService, TodosFacade],
  exports: [RouterModule],
})
export class TodoModule {
}
