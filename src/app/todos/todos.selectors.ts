import {createFeatureSelector, createSelector} from '@ngrx/store';
import {ITodosState} from './reducer';

export const getTodoStore = createFeatureSelector<ITodosState>('todos');

export const getAllTodosStore = createSelector(getTodoStore, t => t.todos);
