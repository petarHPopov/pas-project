import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { switchMap, map, catchError} from 'rxjs/operators';
import {TodoService} from '../core/services/todo.service';
import * as todoActions from './actions';
import {of} from 'rxjs';
import {Action} from '@ngrx/store';

class EffectError implements Action {
  readonly type = '[Error] Effect Error TodoEffects';
}

@Injectable()
export class TodoEffects {

  constructor(
    private actions$: Actions,
    private todosService: TodoService,
  ) { }

  @Effect() todos$ = this.actions$.pipe(
    ofType(todoActions.actionAllTodos),
    switchMap(() =>  this.todosService.getAllTodos()
      .pipe(
        map((todos) => {
          return todoActions.actionAllTodosSuccess({ todos });
        }),
        catchError((err) => of(new EffectError()))
      ))
    );
}

