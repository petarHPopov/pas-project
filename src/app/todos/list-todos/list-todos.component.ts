import {Component, OnDestroy, OnInit} from '@angular/core';
import {Todo} from '../../types/todo.model';
import {select, Store} from '@ngrx/store';
import * as fromActions from '../actions';
import {CreateTodo} from '../../types/create.todo';
import {Observable, Subscription} from 'rxjs';
import {getAllTodosStore} from '../todos.selectors';


@Component({
  selector: 'app-list-todos',
  templateUrl: './list-todos.component.html',
  styleUrls: ['./list-todos.component.css']
})
export class ListTodosComponent implements OnInit, OnDestroy {
  todos$: Observable<Todo[]> = this.store.pipe(select(getAllTodosStore));
  todoSub: Subscription = Subscription.EMPTY;

  allTodo: Todo[] = [];

  constructor(private store: Store) {
  }

  ngOnInit(): void {

    this.store.dispatch(fromActions.actionAllTodos());


    this.todoSub = this.todos$.subscribe(data => {
      this.allTodo = data;
    });
  }

  public created(newTodo: CreateTodo): void {
    newTodo.id = this.allTodo.length ? this.allTodo[this.allTodo.length - 1].id + 1 : 0;

    const newTodoBody: Todo = {
      id: newTodo.id,
      title: newTodo.name,
      completed: newTodo.isDone,
      userId: 3
    };

    this.allTodo = [...this.allTodo, newTodoBody];
  }

  public deleted(todoDeleted: Todo): void {
    this.allTodo = this.allTodo.filter(todo => todo.id !== todoDeleted.id);
  }


  // tslint:disable-next-line:typedef
  public trackTodo(index, todo) {
    return todo ? todo.id : undefined;
  }

  ngOnDestroy(): void {
    this.todoSub.unsubscribe();
  }

}
