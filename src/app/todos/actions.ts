import {createAction, props} from '@ngrx/store';
import {Todo} from '../types/todo.model';

export const actionAllTodos = createAction(
  '[Todos] All Todo'
);

export const actionAllTodosSuccess = createAction(
  '[Todos] All Todo SUCCESS',
  props<{ todos: Todo[] }>()
);
// export const actionAllTodosFailed = createAction(
//   '[Todos] All Todo Failed',
//   props<{ error: any }>()
// );
