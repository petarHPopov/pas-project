import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CreateTodo} from '../../types/create.todo';

@Component({
  selector: 'app-create-todo',
  templateUrl: './create-todo.component.html',
  styleUrls: ['./create-todo.component.css']
})
export class CreateTodoComponent implements OnInit {

  public todoName: string;
  public todoDate: string;

  @Output() createdTodo: EventEmitter<CreateTodo> = new EventEmitter<CreateTodo>();

  constructor() {
  }

  ngOnInit(): void {
  }

  public create(): void {
    const todoCreate = {
      name: this.todoName,
      due: this.todoDate,
      isDone: false,
    };
    this.createdTodo.emit(todoCreate);

    this.todoName = '';
    this.todoDate = '';
  }
}
