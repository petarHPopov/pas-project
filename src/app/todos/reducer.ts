import {Todo} from '../types/todo.model';
import {createReducer, on} from '@ngrx/store';
import * as todoActions from './actions';


export interface ITodosState {
  todos: Todo[];
}

export const initialState: ITodosState = {
  todos: undefined,
};

export const todoReducers = createReducer(
  initialState,
  on(todoActions.actionAllTodos, (state) => {
    return {...state};
  }),
  on(todoActions.actionAllTodosSuccess, (state, todos) => {
    return {...state, ...todos};
  })
);

