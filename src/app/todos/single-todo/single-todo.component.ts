import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Todo} from '../../types/todo.model';

@Component({
  selector: 'app-single-todo',
  templateUrl: './single-todo.component.html',
  styleUrls: ['./single-todo.component.css']
})
export class SingleTodoComponent {

  @Input('info')
  todo: Todo;

  @Output()
  deleteTodo: EventEmitter<Todo> = new EventEmitter<Todo>();

  todoDelete(): void {
    this.deleteTodo.emit(this.todo);
  }

}
